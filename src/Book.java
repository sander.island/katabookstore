public class Book {
    String name = "";
    int price = 0;

    public Book(String name, int price)
    {
        this.name = name;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
