import java.lang.reflect.Array;
import java.util.ArrayList;

public class Main {
    static double discount;
    static double totalPrice = 0;
    static ArrayList<Book> books = new ArrayList<>();
    static ArrayList<Book> purchaseBooks = new ArrayList<>();
    public static void main(String[] args) {

        // Create books
        Book book1 = new Book("Twilight1", 8);
        Book book2 = new Book("Twilight2", 8);
        Book book3 = new Book("Twilight3", 8);
        Book book4 = new Book("Twilight4", 8);
        Book book5 = new Book("HarryPotter1", 8);
        Book book6 = new Book("HarryPotter2", 8);

        // Add books to ArrayList
        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        books.add(book5);
        books.add(book6);

        // Add books to purchase
        purchaseBook(book1);
        purchaseBook(book2);
        purchaseBook(book3);

        totalPrice(purchaseBooks);
        System.out.println("Original price: " + getPrice(purchaseBooks) + " EUR");
        System.out.println("Discount: " + discount * 100 + "%");
        System.out.println("Discounted price: " + totalPrice(purchaseBooks) + " EUR");

    }

    // Adds a book to the purchaseBooks list
    public static void purchaseBook(Book book)
    {
        purchaseBooks.add(book);
    }

    // Calculates the discount
    public static double calculateDiscount(ArrayList list)
    {
        int twilightBooks = 0;
        if (list.size() > 1)
        {
            for (Book book : purchaseBooks) {
                if (book.name.contains("Twilight"))
                {
                    twilightBooks ++;
                }
            }

            if (twilightBooks == 2)
            {
                discount = 0.05;
            }
            else if (twilightBooks == 3)
            {
                discount = 0.1;
            }
            else if (twilightBooks == 4)
            {
                discount = 0.2;
            }
        }
        return discount;
    }

    // Adding the discount to the total price
    public static double totalPrice(ArrayList list)
    {
        int booksPrice = 0;
        for (Book book : purchaseBooks)
        {
            booksPrice += book.getPrice();
        }
        totalPrice = booksPrice - (booksPrice * (calculateDiscount(purchaseBooks)));
        return totalPrice;
    }

    public static int getPrice(ArrayList list)
    {
        int booksPrice = 0;
        for (Book book : purchaseBooks)
        {
            booksPrice += book.getPrice();
        }
        return booksPrice;
    }
}